package com.binarystudio.academy.springsecurity.security.jwt;

public final class JwtClaims {
    /**
     * Indicates that this token is used for refreshing access tokens.
     */
    public static final String REFRESH = "refresh";
}
