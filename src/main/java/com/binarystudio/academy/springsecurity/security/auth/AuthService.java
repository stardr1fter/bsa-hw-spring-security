package com.binarystudio.academy.springsecurity.security.auth;

import com.binarystudio.academy.springsecurity.domain.user.UserService;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.exceptions.InvalidResetPasswordTokenException;
import com.binarystudio.academy.springsecurity.security.auth.model.*;
import com.binarystudio.academy.springsecurity.security.jwt.JwtProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.BadJwtException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@Service
public class AuthService {
	private final UserService userService;
	private final JwtProvider jwtProvider;
	private final PasswordEncoder passwordEncoder;

	public AuthService(UserService userService, JwtProvider jwtProvider, PasswordEncoder passwordEncoder) {
		this.userService = userService;
		this.jwtProvider = jwtProvider;
		this.passwordEncoder = passwordEncoder;
	}

	public AuthResponse performLogin(AuthorizationRequest authorizationRequest) {
		var userDetails = userService.loadUserByUsername(authorizationRequest.getUsername());
		if (passwordsDontMatch(authorizationRequest.getPassword(), userDetails.getPassword())) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid password");
		}
		return AuthResponse.of(
				jwtProvider.generateAccessToken(userDetails),
				jwtProvider.generateRefreshToken(userDetails));
	}

	public AuthResponse performRegistration(RegistrationRequest registrationRequest) {
		var newUser = userService.createUserByEmailAndPassword(
				registrationRequest.getLogin(),
				registrationRequest.getEmail(),
				registrationRequest.getPassword());

		return performLogin(new AuthorizationRequest(
				registrationRequest.getLogin(),
				registrationRequest.getPassword()));
	}

	public AuthResponse refreshTokenPair(RefreshTokenRequest refreshRequest) {
		var token = refreshRequest.getRefreshToken();

		if (!jwtProvider.isRefreshToken(token)) {
			throw new BadJwtException("Provided JWT was not meant for refreshing.");
		}

		var login = jwtProvider.getLoginFromToken(token);
		var userDetails = userService.loadUserByUsername(login);
		return generateTokens(userDetails);
	}

	public AuthResponse changePassword(PasswordChangeRequest passwordChangeRequest, Authentication authentication) {
		var user = userService.changePassword(authentication.getName(), passwordChangeRequest.getNewPassword());
		// but old unexpired tokens would still remain valid
		return generateTokens(user);
	}

	public void sendResetPasswordToken(String email) {
		var resetToken = userService.createResetPasswordToken(email);
		log.info("#".repeat(60));
		log.info("\nPretending this token was sent to '{}':\n{}\n", email, resetToken);
		log.info("#".repeat(60));
	}

	public AuthResponse resetPasswordByToken(ForgottenPasswordReplacementRequest request) {
		return userService.findEligibleForPasswordReset(request.getToken())
				.map(u -> {
					userService.changePassword(u.getUsername(), request.getNewPassword());
					return generateTokens(u);
				})
				.orElseThrow(InvalidResetPasswordTokenException::new);
	}

	private AuthResponse generateTokens(User user) {
		return AuthResponse.of(
				jwtProvider.generateAccessToken(user),
				jwtProvider.generateRefreshToken(user));
	}

	private boolean passwordsDontMatch(String rawPw, String encodedPw) {
		return !passwordEncoder.matches(rawPw, encodedPw);
	}
}
