package com.binarystudio.academy.springsecurity.security.jwt;

import com.binarystudio.academy.springsecurity.domain.user.model.User;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Map;

@Component
public class JwtProvider {
	private final JwtProperties jwtProperties;
	private Key secretKey;
	private JwtParser jwtParser;

	@Autowired
	public JwtProvider(JwtProperties jwtProperties) {
		this.jwtProperties = jwtProperties;
	}

	private Key key() {
		if (secretKey == null) {
			byte[] keyBytes = Decoders.BASE64.decode(jwtProperties.getSecret());
			secretKey = Keys.hmacShaKeyFor(keyBytes);
		}
		return secretKey;
	}

	private JwtParser jwtParser() {
		if (jwtParser == null) {
			jwtParser = Jwts.parserBuilder().setSigningKey(key()).build();
		}
		return jwtParser;
	}

	public String generateRefreshToken(User user) {
		var expiresInSeconds = jwtProperties.getSecs_to_expire_refresh();
		var expiration = createExpirationDateFromSecondsOffset(expiresInSeconds);
		return generateToken(user, expiration, Map.of(JwtClaims.REFRESH, true));
	}

	public String generateAccessToken(User user) {
		var expiresInSeconds = jwtProperties.getSecs_to_expire_access();
		var expiration = createExpirationDateFromSecondsOffset(expiresInSeconds);
		return generateToken(user, expiration, Map.of());
	}

	public String getLoginFromToken(String token) {
		Claims claims = parseToken(token);
		return claims.getSubject();
	}

	public boolean isRefreshToken(String token) {
		Claims claims = parseToken(token); // maybe cache parsed tokens per request?
		var claim = claims.get(JwtClaims.REFRESH, Boolean.class);
		return claim != null && claim;
	}

	private String generateToken(User user, Date expiration, Map<String, Object> claims) {
		return Jwts.builder()
				.setSubject(user.getUsername())
				.addClaims(claims)
				.setExpiration(expiration)
				.signWith(key())
				.compact();
	}

	private Date createExpirationDateFromSecondsOffset(long secondsSinceNow) {
		OffsetDateTime utc = OffsetDateTime.now(ZoneOffset.UTC).plusSeconds(secondsSinceNow);
		return Date.from(utc.toInstant());
	}

	private Claims parseToken(String token) {
		try {
			return jwtParser().parseClaimsJws(token).getBody();
		} catch (ExpiredJwtException expEx) {
			throw new JwtException("Token expired", "jwt-expired");
		} catch (UnsupportedJwtException unsEx) {
			throw new JwtException("Unsupported jwt", "jwt-unsupported");
		} catch (MalformedJwtException mjEx) {
			throw new JwtException("Malformed jwt", "jwt-malformed");
		} catch (SignatureException sEx) {
			throw new JwtException("Invalid signature", "jwt-signature");
		} catch (Exception e) {
			throw new JwtException("Invalid token", "jwt-invalid");
		}
	}
}
