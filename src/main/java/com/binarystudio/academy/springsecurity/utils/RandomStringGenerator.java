package com.binarystudio.academy.springsecurity.utils;

public interface RandomStringGenerator {
    String generate(int length);
}
