package com.binarystudio.academy.springsecurity.utils;

import io.jsonwebtoken.io.Encoders;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;

@Component
public class SecureRandomStringGenerator implements RandomStringGenerator {
    @Override
    public String generate(int length) {
        var random = new SecureRandom();
        byte[] bytes = new byte[length / 4 * 3];
        random.nextBytes(bytes);
        return Encoders.BASE64.encode(bytes);
    }
}
