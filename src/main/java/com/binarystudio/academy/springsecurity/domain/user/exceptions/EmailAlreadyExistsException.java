package com.binarystudio.academy.springsecurity.domain.user.exceptions;

public class EmailAlreadyExistsException extends RuntimeException {
    public EmailAlreadyExistsException(String email) {
        super(String.format("User with such email (%s) already exists.", email));
    }
}