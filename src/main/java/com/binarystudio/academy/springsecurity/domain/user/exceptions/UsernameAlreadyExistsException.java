package com.binarystudio.academy.springsecurity.domain.user.exceptions;

public class UsernameAlreadyExistsException extends RuntimeException {
    public UsernameAlreadyExistsException(String username) {
        super(String.format("User with such name (%s) already exists.", username));
    }
}
