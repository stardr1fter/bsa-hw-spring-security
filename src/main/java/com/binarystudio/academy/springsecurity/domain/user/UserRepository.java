package com.binarystudio.academy.springsecurity.domain.user;

import com.binarystudio.academy.springsecurity.domain.hotel.model.Hotel;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.domain.user.model.UserRole;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserRepository {
	private final List<User> users = new ArrayList<>();

	private final PasswordEncoder passwordEncoder;

	public UserRepository(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;

		var regularUser = new User();
		regularUser.setUsername("regular");
		regularUser.setEmail("regular@mail.com");
		regularUser.setId(UUID.randomUUID());
		regularUser.setPassword(passwordEncoder.encode("password"));
		regularUser.setAuthorities(Set.of(UserRole.USER));
		this.users.add(regularUser);

		var adminUser = new User();
		adminUser.setUsername("privileged");
		adminUser.setEmail("privileged@mail.com");
		adminUser.setId(UUID.randomUUID());
		adminUser.setPassword(passwordEncoder.encode("password"));
		adminUser.setAuthorities(Set.of(UserRole.ADMIN));
		this.users.add(adminUser);
	}

	public Optional<User> getById(UUID id) {
		return users.stream()
				.filter(u -> u.getId().equals(id))
				.findAny();
	}

	public Optional<User> findByUsername(String username) {
		return users.stream().filter(user -> user.getUsername().equals(username)).findAny();
	}

	public Optional<User> findByEmail(String email) {
		return users.stream().filter(user -> user.getEmail().equals(email)).findAny();
	}

	public Optional<User> findByResetPasswordToken(String resetPasswordToken) {
		return users.stream()
				.filter(u -> u.getResetPasswordToken() != null)
				.filter(u -> u.getResetPasswordToken().equals(resetPasswordToken))
				.findAny();
	}

	public List<User> findUsers() {
		return Collections.unmodifiableList(users);
	}

	public User createUserByEmail(String email) {
		var createdUser = new User();
		createdUser.setId(UUID.randomUUID());
		createdUser.setEmail(email);
		createdUser.setUsername(email);
		createdUser.setAuthorities(Set.of(UserRole.USER));
		users.add(createdUser);
		return createdUser;
	}

	public User createUser(String username, String email, String rawPassword) {
		var createdUser = new User();
		createdUser.setId(UUID.randomUUID());
		createdUser.setEmail(email);
		createdUser.setUsername(username);
		createdUser.setPassword(passwordEncoder.encode(rawPassword));
		createdUser.setAuthorities(Set.of(UserRole.USER));
		users.add(createdUser);
		return createdUser;
	}

	public User updatePassword(String username, String rawPassword) {
		var foundUser = findByUsername(username).orElseThrow();
		foundUser.setPassword(passwordEncoder.encode(rawPassword));
		return foundUser;
	}

	public User updateResetPasswordToken(String username, String resetPasswordToken) {
		var foundUser = findByUsername(username).orElseThrow();
		foundUser.setResetPasswordToken(resetPasswordToken);
		return foundUser;
	}
}
