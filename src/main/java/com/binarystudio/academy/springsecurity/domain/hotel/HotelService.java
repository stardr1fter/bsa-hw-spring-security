package com.binarystudio.academy.springsecurity.domain.hotel;

import com.binarystudio.academy.springsecurity.domain.hotel.model.Hotel;
import com.binarystudio.academy.springsecurity.domain.user.UserService;
import com.binarystudio.academy.springsecurity.domain.user.model.UserRole;
import com.binarystudio.academy.springsecurity.exceptions.OperationForbiddenException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Service
@Slf4j
public class HotelService {
	private final HotelRepository hotelRepository;

	private final UserService userService;

	public HotelService(HotelRepository hotelRepository, UserService userService) {
		this.hotelRepository = hotelRepository;
		this.userService = userService;
	}

	public void delete(UUID hotelId, Authentication authentication) {
		if (!canModifyHotel(hotelId, authentication)) {
			log.warn("Unauthorized attempt to delete hotel '{}' by user '{}'", hotelId, authentication.getName());
			throw new OperationForbiddenException("The user is not authorized to delete the hotel.");
		}

		boolean wasDeleted = hotelRepository.delete(hotelId);
		if (!wasDeleted) {
			throw new NoSuchElementException();
		}
	}

	public List<Hotel> getAll() {
		return hotelRepository.getHotels();
	}


	public Hotel update(Hotel hotel, Authentication authentication) {
		if (!canModifyHotel(hotel, authentication)) {
			log.warn("Unauthorized attempt to update hotel '{}' by user '{}'", hotel.getId(), authentication.getName());
			throw new OperationForbiddenException("The user is not authorized to update the hotel.");
		}

		getById(hotel.getId());
		return hotelRepository.save(hotel);
	}

	public Hotel create(Hotel hotel, Authentication authentication) {
		var user = userService.getFromPrincipal(authentication.getPrincipal());
		hotel.setOwnerId(user.getId());
		return hotelRepository.save(hotel);
	}

	public Hotel getById(UUID hotelId) {
		return hotelRepository.getById(hotelId).orElseThrow();
	}

	private boolean canModifyHotel(Hotel hotel, Authentication authentication) {
		return canModifyHotel(hotel.getId(), authentication);
	}

	private boolean canModifyHotel(UUID hotelId, Authentication authentication) {
		if (authentication.getAuthorities().contains(UserRole.USER)) {
			var requester = userService.getFromPrincipal(authentication.getPrincipal());
			var hotel = hotelRepository.getById(hotelId).orElseThrow();
			return requester.getId().equals(hotel.getOwnerId()); // owns the hotel
		}

		return authentication.getAuthorities().contains(UserRole.ADMIN);
	}
}
