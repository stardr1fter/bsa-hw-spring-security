package com.binarystudio.academy.springsecurity.domain.user;

import com.binarystudio.academy.springsecurity.domain.user.exceptions.EmailAlreadyExistsException;
import com.binarystudio.academy.springsecurity.exceptions.InvalidResetPasswordTokenException;
import com.binarystudio.academy.springsecurity.domain.user.exceptions.UsernameAlreadyExistsException;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.utils.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
	private static final int RESET_PASSWORD_TOKEN_LENGTH = 256;

	private final UserRepository userRepository;
	private final RandomStringGenerator randomString;

	@Autowired
	public UserService(UserRepository userRepository, RandomStringGenerator randomString) {
		this.userRepository = userRepository;
		this.randomString = randomString;
	}

	@Override
	public User loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Not found"));
	}

	public List<User> getAll() {
		return userRepository.findUsers();
	}

	public User createUserByEmail(String email) {
		throwIfEmailExists(email);
		return userRepository.createUserByEmail(email);
	}

	public User createUserByEmailAndPassword(String username, String email, String rawPassword) {
		throwIfEmailExists(email);
		throwIfUsernameExists(email);
		return userRepository.createUser(username, email, rawPassword);
	}

	public User changePassword(String username, String newRawPassword) {
		return userRepository.updatePassword(username, newRawPassword);
	}

	public String createResetPasswordToken(String email) {
		var user = userRepository.findByEmail(email).orElseThrow();
		var token = randomString.generate(RESET_PASSWORD_TOKEN_LENGTH);
		userRepository.updateResetPasswordToken(user.getUsername(), token);
		return token;
	}

	public Optional<User> findEligibleForPasswordReset(String resetToken) {
		return userRepository.findByResetPasswordToken(resetToken);
	}

	public User getFromPrincipal(Object principal) {
		if (principal instanceof  User) {
			return (User)principal;
		}
		if (principal instanceof UserDetails) {
			return loadUserByUsername(((UserDetails) principal).getUsername());
		}
		throw new AuthenticationException("Unknown principal " + principal.getClass()) {
			@Override
			public String getMessage() {
				return super.getMessage();
			}
		};
	}

	private void throwIfEmailExists(String email) {
		if (userRepository.findByEmail(email).isPresent()) {
			throw new EmailAlreadyExistsException(email);
		}
	}

	private void throwIfUsernameExists(String username) {
		if (userRepository.findByUsername(username).isPresent()) {
			throw new UsernameAlreadyExistsException(username);
		}
	}
}
