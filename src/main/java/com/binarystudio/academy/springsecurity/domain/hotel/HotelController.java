package com.binarystudio.academy.springsecurity.domain.hotel;

import com.binarystudio.academy.springsecurity.domain.hotel.model.Hotel;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("hotels")
public class HotelController {
	private final HotelService hotelService;

	public HotelController(HotelService hotelService) {
		this.hotelService = hotelService;
	}

	@GetMapping("all")
	public List<Hotel> getHotels() {
		return hotelService.getAll();
	}

	@DeleteMapping("delete/{hotelId}")
	public void deleteHotel(@PathVariable UUID hotelId, Authentication authentication) {
		hotelService.delete(hotelId, authentication);
	}

	@PutMapping("create")
	public Hotel createHotel(@RequestBody Hotel hotel, Authentication authentication) {
		return hotelService.create(hotel, authentication);
	}

	@PatchMapping("update")
	public Hotel updateHotel(@RequestBody Hotel hotel, Authentication authentication) {
		return hotelService.update(hotel, authentication);
	}

	@GetMapping("{hotelId}")
	public Hotel getHotel(@PathVariable UUID hotelId) {
		return hotelService.getById(hotelId);
	}
}
