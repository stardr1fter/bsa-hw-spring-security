package com.binarystudio.academy.springsecurity.exceptions;

public class InvalidResetPasswordTokenException extends RuntimeException {
    public InvalidResetPasswordTokenException() {
        super("The token has expired or never existed.");
    }
}
